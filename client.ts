import { HelloRequest, HelloReply } from './grpc-web/helloworld_pb'
import { GreeterClient, ServiceError } from './grpc-web/helloworld_pb_service'
import { grpc } from '@improbable-eng/grpc-web'
import { NodeHttpTransport } from '@improbable-eng/grpc-web-node-http-transport';

const client = new GreeterClient('http://envoy:50051', {
    transport: NodeHttpTransport()
})

const requestMessage = new HelloRequest()
requestMessage.setName("grpc-web!!")

client.sayHello(requestMessage, new grpc.Metadata(), (error: ServiceError | null, reply: HelloReply | null) => {
    if (error != null) {
        console.log('code: %d, message: %s.', error.code, error.message)
    } else if (reply !== null && error == null) {
        console.log('message: %s.', reply.getMessage())
    }
})
