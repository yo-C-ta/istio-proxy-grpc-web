FROM golang:1.15 AS build-env

RUN cd $GOPATH/src && git clone -b v1.34.0 https://github.com/grpc/grpc-go
WORKDIR $GOPATH/src/grpc-go/examples/helloworld

RUN go get -d ./greeter_server/...
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /bin/greeter_server ./greeter_server/main.go

FROM alpine:latest
COPY --from=build-env /bin/greeter_server /bin/
CMD ["/bin/greeter_server"]
