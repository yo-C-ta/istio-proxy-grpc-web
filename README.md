# istio-proxy-grpc-web

Sample grpc-web server via istio-proxy

## Generate client code

```bash
npm install ts-protoc-gen

protoc -I. \
    --plugin="protoc-gen-ts=./node_modules/.bin/protoc-gen-ts" \
    --js_out="import_style=commonjs,binary:./grpc-web" \
    --ts_out="service=grpc-web:./grpc-web" \
    ./helloworld.proto
```
